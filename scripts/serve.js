// require the module as normal
const browserSync = require('browser-sync');

// Start the server
browserSync({
    server: ['src','.'],
    files: [
        'srcjs/*js',
        'src/css/*.css',
        'src/*.html',
    ],
});
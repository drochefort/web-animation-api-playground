const fs = require('fs');
const Vulcanize = require('vulcanize');

const { elements } = require('../elements.json');

const vulcan = new Vulcanize({
    inlineScripts: true,
    inlineCss: true,
});

function exists(dir) {
    try {
        const stat = fs.statSync(dir);
        return stat.isDirectory();
    } catch (e) {
        return false;
    }
}


elements.forEach((el) => {
    vulcan.process(`src/${el}/${el}.html`, (err, html) => {
        if (!exists('build')) {
            fs.mkdirSync('build');
        }

        if (err){
            throw err;
        }

        fs.writeFileSync(`build/${el}.html`, html);
    });
});


